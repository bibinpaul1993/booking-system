from django.db import models

# Create your models here.

from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager

class Users(AbstractUser):
    email = models.EmailField(verbose_name='email', max_length=100, unique=True)
    username = models.CharField(max_length=250, unique=True)
    name = models.CharField(max_length=250)
    subdomain = models.CharField(max_length=250)
    theme = models.CharField(max_length=250)
    password = models.CharField(max_length=250)

